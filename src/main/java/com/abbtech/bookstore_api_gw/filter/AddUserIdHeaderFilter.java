package com.abbtech.bookstore_api_gw.filter;

import com.abbtech.bookstore_api_gw.config.CustomHttpServletRequestWrapper;
import com.abbtech.bookstore_api_gw.dto.request.ValidationRequestDto;
import com.abbtech.bookstore_api_gw.dto.request.TokenRequest;
import com.abbtech.bookstore_api_gw.dto.response.TokenResponse;
import com.abbtech.bookstore_api_gw.feign.AuthServiceClient;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.Objects;

@Component
@RequiredArgsConstructor
public class AddUserIdHeaderFilter extends OncePerRequestFilter {

    private final AuthServiceClient authServiceClient;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String path = request.getRequestURI();
        String method = request.getMethod();
        String authHeader = request.getHeader("Authorization");
        ValidationRequestDto validationRequestDto = new ValidationRequestDto(path, method, authHeader);

        Boolean check = authServiceClient.checkAccess(validationRequestDto);
        if(Boolean.FALSE.equals(check)){
            authServiceClient.checkAuth(validationRequestDto);

            TokenRequest tokenRequest = new TokenRequest(authHeader);
            TokenResponse resp = authServiceClient.parseToken(tokenRequest);

            CustomHttpServletRequestWrapper requestWrapper = new CustomHttpServletRequestWrapper(request);
            requestWrapper.addHeader("X-USER-ID", Objects.requireNonNull(resp).userId());
            requestWrapper.addHeader("X-USER-EMAIL", resp.userEmail());

            filterChain.doFilter(requestWrapper, response);
            return;
        }
        filterChain.doFilter(request, response);
    }
}
