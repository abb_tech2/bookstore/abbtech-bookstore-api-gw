package com.abbtech.bookstore_api_gw.feign;

import com.abbtech.bookstore_api_gw.dto.request.ValidationRequestDto;
import com.abbtech.bookstore_api_gw.dto.request.TokenRequest;
import com.abbtech.bookstore_api_gw.dto.response.TokenResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "auth-service", url = "${ms.user.url}")
public interface AuthServiceClient {

    @PostMapping("/checkAuth")
    String checkAuth(@RequestBody ValidationRequestDto validationRequestDto);

    @PostMapping("/checkAccess")
    Boolean checkAccess(@RequestBody ValidationRequestDto validationRequestDto);

    @PostMapping("/parseToken")
    TokenResponse parseToken(@RequestBody TokenRequest tokenRequest);


}