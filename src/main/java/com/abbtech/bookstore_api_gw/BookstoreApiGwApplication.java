package com.abbtech.bookstore_api_gw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class BookstoreApiGwApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookstoreApiGwApplication.class, args);
	}

}
