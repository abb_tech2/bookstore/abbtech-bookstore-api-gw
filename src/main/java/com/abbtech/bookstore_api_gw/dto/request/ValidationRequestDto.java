package com.abbtech.bookstore_api_gw.dto.request;

public record ValidationRequestDto(String path, String method, String authHeader){
}
