package com.abbtech.bookstore_api_gw.dto.response;

public record
TokenResponse(String userId, String userEmail) {
}
