package com.abbtech.bookstore_api_gw.dto.request;

public record TokenRequest(String token) {
}
